const service = require('../index');

describe('AuthService test suite', () => {

  let user;

  beforeAll(async () => {
    const pw = await service.generatePassword('password');
    user = {username: 'username', password: pw};
  });

  it('should init with defaults if config not correctly', () => {
    service.init();
    expect(service.config.ACCESS_TOKEN_SECRET).toEqual('DefaultAccessTokenSecret');
    expect(service.config.REFRESH_TOKEN_SECRET).toEqual('DefaultRefreshTokenSecret');
  });

  it('should init with correct secrets', () => {
    service.init({ACCESS_TOKEN_SECRET: '12345678', REFRESH_TOKEN_SECRET: '87654321'});
    expect(service.config.ACCESS_TOKEN_SECRET).toEqual('12345678');
    expect(service.config.REFRESH_TOKEN_SECRET).toEqual('87654321');
  });

  it('should return null if user not provided', async () => {
    const result = await service.authenticate('username', 'password', null);
    expect(result).toBeFalsy();
  });

  it('should return null if authentication fails', async () => {
    const result = await service.authenticate('username', 'password', {password: 'password'});
    expect(result).toBeFalsy();
  });

  it('should return user if authentication succeeded', async () => {
    const result = await service.authenticate('username', 'password', user);
    expect(result).toBeTruthy();
    expect(result.username).toEqual('username');
  });

  it('should generate access token', () => {
    const token = service.generateAccessToken({username: 'username', password: 'password'});
    expect(token).toBeTruthy();
  });

  it('should generate refresh token', () => {
    const token = service.generateRefreshToken({username: 'username', password: 'password'});
    expect(token).toBeTruthy();
  });

  it('should should give unauthorized if no token provided', () => {

    const req = {headers: {}};
    const res = {sendStatus: (status) => status};

    const response = service.authenticateToken(req, res, () => {});
    expect(response).toEqual(401);
  });

  it('should should give unauthorized for false token', () => {

    const req = {headers: {authorization: 'abcd'}};
    const res = {sendStatus: (status) => {
        expect(status).toEqual(401);
        return status;
      }};

    const response = service.authenticateToken(req, res, () => {});
    expect(response).toEqual(401);
  });

  it('should should give forbidden for false token', () => {

    const req = {headers: {authorization: 'Bearer abcd'}};
    const res = {sendStatus: (status) => {
        expect(status).toEqual(403);
      }};

    service.authenticateToken(req, res, () => {});
  });

  it('should should success with user', () => {

    const token = service.generateAccessToken({username: 'username'});

    const req = {headers: {authorization: `Bearer ${token}`}};
    const next = () => {
      expect(req.user).toBeTruthy();
      expect(req.user.username).toEqual('username');
    };

    service.authenticateToken(req, {}, next);
  });

  describe('authenticateRole tests', () => {

    let res;

    beforeEach(() => {
      res = {
        status: (status) => {
          res['status'] = status;
          return res;
        },
        send: (message) => {
          res['message'] = message;
          return res;
        }
      };
    });

    it('should return unauthorized if no user', () => {
      const req = {};
      const res = {sendStatus: (status) => {
          expect(status).toEqual(401);
          return status;
        }};

      const response = service.authenticateRole('ROLE_ADMIN')(req, res, () => {});
      expect(response).toEqual(401);
    });

    it('should return forbidden if no role', () => {
      const req = {user: {}};

      const response = service.authenticateRole('ROLE_ADMIN')(req, res, () => {});
      expect(response.status).toEqual(403);
      expect(response.message).toEqual('Not allowed');
    });

    it('should return forbidden if wrong role', () => {
      const req = {user: {roles: ['ROLE_BASIC']}};

      const response = service.authenticateRole('ROLE_ADMIN')(req, res, () => {});
      expect(response.status).toEqual(403);
      expect(response.message).toEqual('Not allowed');
    });

    it('should continue if correct role', () => {
      const req = {user: {roles: ['ROLE_BASIC', 'ROLE_ADMIN']}};

      service.authenticateRole('ROLE_ADMIN')(req, res, () => {
        expect(true).toBeTruthy();
      });
    });
  });

  it('should verify refresh token', () => {
    const token = service.generateRefreshToken({username: 'Dennowar'});
    service.verifyRefreshToken(token, (err, user) => {
      expect(err).toBeFalsy();
      expect(user.username).toEqual('Dennowar');
    })
  });

});
