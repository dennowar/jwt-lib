// @ts-ignore
import {RequestHandler} from "express";
// @ts-ignore
export = new AuthService();

declare class AuthService {

    init(config: {ACCESS_TOKEN_SECRET?: string, REFRESH_TOKEN_SECRET?: string}): void;
    authenticateToken(): RequestHandler;
    authenticateRole(role: string): RequestHandler;
}
