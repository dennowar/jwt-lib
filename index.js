const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

class AuthService {
   _config = {
      ACCESS_TOKEN_SECRET: 'DefaultAccessTokenSecret',
      REFRESH_TOKEN_SECRET: 'DefaultRefreshTokenSecret',
      ACCESS_TOKES_EXPIRATION: '1h',
      REFRESH_TOKEN_EXPIRATION: '24h'
   };

   init(config) {
      this._config.ACCESS_TOKEN_SECRET = config && config.ACCESS_TOKEN_SECRET ? `${config.ACCESS_TOKEN_SECRET}` : 'DefaultAccessTokenSecret';
      this._config.REFRESH_TOKEN_SECRET = config && config.REFRESH_TOKEN_SECRET ? `${config.REFRESH_TOKEN_SECRET}` : 'DefaultRefreshTokenSecret';
      this._config.ACCESS_TOKES_EXPIRATION = config && config.ACCESS_TOKES_EXPIRATION ? `${config.ACCESS_TOKES_EXPIRATION}` : '1h';
      this._config.REFRESH_TOKEN_EXPIRATION = config && config.REFRESH_TOKEN_EXPIRATION ? `${config.REFRESH_TOKEN_EXPIRATION}` : '24h';
   }

   async authenticate(username, password, user) {

      if (!user) {
         return null;
      }

      try {
         return await bcrypt.compare(password, user.password) ? user : null;
      } catch {
         return null;
      }
   }

   generateAccessToken(user) {
      return jwt.sign(user, this._config.ACCESS_TOKEN_SECRET, {expiresIn: '1h'});
   }

   generateRefreshToken(user) {
      return jwt.sign(user, this._config.REFRESH_TOKEN_SECRET, {expiresIn: '24h'});
   }

   get authenticateToken() {
      return (req, res, next) => {
         const authHeader = req.headers['authorization'];
         const token = authHeader && authHeader.split(' ')[1];

         if (!token) {
            return res.sendStatus(401);
         }

         jwt.verify(token, this._config.ACCESS_TOKEN_SECRET, (err, user) => {
            if (err) {
               return res.sendStatus(403);
            }
            req.user = {...user, roles: JSON.parse(user.roles)};
            next();
         });
      };
   }

   authenticateRole(role) {
      return (req, res, next) => {

         if (!req.user) {
            return res.sendStatus(401);
         }

         const user = req.user;

         if (!user.roles || (!user.roles.includes(role) && !user.roles.includes('admin'))) {
            return res.status(403).send('Not allowed');
         }
         next();
      }
   }

   authenticateRoles(roles) {
      return (req, res, next) => {

         if (!req.user) {
            return res.sendStatus(401);
         }
         const user = req.user;
         const hasRole = roles.some(role => user.roles.includes(role));

         if (!user.roles || (!hasRole && !user.roles.includes('admin'))) {
            return res.status(403).send('Not allowed');
         }
         next();
      }
   }

   verifyRefreshToken(refreshToken, callback) {
      jwt.verify(refreshToken, this._config.REFRESH_TOKEN_SECRET, callback);
   }

   async generatePassword(password) {
      return await bcrypt.hash(password, 10);
   }

   get config() {
      return this._config;
   }
}

module.exports = new AuthService();
